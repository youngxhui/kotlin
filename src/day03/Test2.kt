package day03

/**
 * 水仙花书
 */
fun main(args: Array<String>) {
    var a = 0
    var b = 0
    var c = 0
    for (i in 100..999) {
        a = i / 100
        b = i / 10 - a * 10
        c = i - b * 10 - a * 100
        if (a * a * a + b * b * b + c * c * c == i) {
            println(i)
        }
    }
}