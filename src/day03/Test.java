package day03;

public class Test {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        int c = 0;
        System.out.println("100到999的水仙花数为：");
        for (int i = 100; i <= 999; i++) {
            a = i / 100;
            b = i / 10 - a * 10;
            c = i - b * 10 - a * 100;
            if (i == a * a * a + b * b * b + c * c * c) {
                System.out.println(i);
            }
        }
    }
}
