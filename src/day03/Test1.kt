package day03

/**
 * 计算101-200的素数
 */
fun main(args: Array<String>) {
    var sum = 0
    print("101到200的素数是： ")
    for (i in 101..200) {
        var flag = 0
        var j = 2
        while (j < Math.sqrt(i.toDouble())) {
            var k: Float = i.toFloat()
            if (k % j == 0f) {
                flag = 1
                break
            }
        }
        if (flag == 0) {
            print("$i  ")
            sum++
        }
    }
    println()
    print("素数个数为：$sum ")


}
