package day01

/**
 * Created by young on 2017/7/5.
 * 常量和变量
 * 常量用 val
 * 常量赋值后无法再赋值。
 */

val π = 3.1415926
val NUM = 1

fun main(args: Array<String>) {
    println(π)
    println(NUM)
}