package day01

/**
 * Created by young on 2017/7/5.
 * 字符串 String 和 字符 Char
 */
fun main(args: Array<String>) {

    //1.
    //用 "" 包裹
    val a = "abcd"

    //是否为空 isEmpty
    println("字符串a是否为空 ${a.isEmpty()}")

    //计数 count
    println("字符串a的长度为 ${a.count()} (count)")

    // 与count一样，没有区别
    /**
     * count的源码就是通过length实现的
     * <code>
     *     @kotlin.internal.InlineOnly
     *   public inline fun CharSequence.count(): Int {
     *      return length
     *   }
     * </code>
     */
    println("字符串a的长度为 ${a.length} (length)")

    //2.
    //char 用 '' 包裹
    val b = 'a'

    //判断是否为字母/文字
    println("b 是否为文字 ${b.isLetter()}")

    val c = '5'

    //判断是否为数字
    println("c 是否为数字 ${c.isDigit()}")

    //3.
    //将字符串转化为字符数组
    for (word in a.toCharArray()) {
        print(word + " ")
    }
}