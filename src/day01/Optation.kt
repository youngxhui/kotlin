package day01

/**
 * Created by young on 2017/7/5.
 * 操作符 + - * /等
 */

fun main(args: Array<String>) {

    //一元操作符
    val a = 3
    val b = -a
    println("b 是 $b")

    //二元操作符
    val d = a + b
    println("d 是 $d")

    //逻辑操作符 与或非
    val haveComputer = true
    val isNetable = false
    if (haveComputer && isNetable) {
        println("你可以学习kotlin了")
    } else {
        println("你还不能学习kotlin")

    }

}