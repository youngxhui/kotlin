package day01

/**
 * Created by young on 2017/7/5.
 * 元组，给多个变量同时赋值。有三元和二元
 */
fun main(args: Array<String>) {

    // 三元
    val (day, method, course) = Triple(3, "学会", "kotlin")
    println("$day 天$method$course")

    // 二元
    val (desc, toll) = Pair("学费", 0)
    println("$desc  $toll")

    //直接命名
    val results = Pair("数学", 100)
    println(results)
}