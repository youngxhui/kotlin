package day01

/**
 * Created by young on 2017/7/6.
 * Array 数组
 * Array 索引从0开始
 */

fun main(args: Array<String>) {

    // Array<类型> 或者 arrayOf()
    val array = arrayOf("中国", "美国")

    for (i in array) {
        println(i)
    }


    val array2 = Array(2, { "中国" })
    for (i2 in array2) {
        println(i2)
    }

    //打印1到100 lambda表达式
    val oneToHundred = Array(100, { i -> i + 1 })
    for (i in oneToHundred) {
        print("$i ")
    }
    println()

    // 计数 count
    println("数组的元素个数为 ${oneToHundred.count()}")

    //获取某个数组中的元素 获取第一个
    println("数组的第一个是 ${oneToHundred.first()}")

    //获取第n个元素
    val n = 6
    println("第6个元素是 ${oneToHundred[n - 1]}")

    //去除重复数组  底层源码 转化为 set集合
    val array3 = arrayOf(1, 2, 3, 4, 5, 6, 4, 3, 2, 1)
    println("去除重复元素${array3.distinct()}")

    //直接转化为set集合
    println("直接转化为set集合${array3.toSet()}")

    //数组切割
    val num22To33 = oneToHundred.sliceArray(21..32)
    for (i in num22To33) {
        println("切割后的数组是$i")
    }

    //mutableList 长度可变，类型不可变，类似于Java 中的list
    val mutableLists = mutableListOf("中国", "美国")
    println("mutableList 是 $mutableLists")

    mutableLists.add("英国")
    println("添加后的mutableList 是 $mutableLists")

    mutableLists.remove("美国")
    println("删除后的mutableList 是 $mutableLists")

}