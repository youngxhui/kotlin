package day01

/**
 * Created by young on 2017/7/6.
 * 流程控制
 */
fun main(args: Array<String>) {
    val result = 1

    when (result) {
        in 0..2 -> {
            println("在0到2")
            println("true")
        }
        in 3..4 -> println("在3到4")
        (9 + 1) -> println("10")
        else -> println("不在已知范围")
    }
}