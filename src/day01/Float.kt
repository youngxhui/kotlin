package day01

/**
 * Created by young on 2017/7/5.
 * Kotlin 浮点，默认为double
 */
fun main(args: Array<String>) {
    var pi = 3.1415926535897932384626

    //圆周率是3.141592653589793 不会全部打印出来
    println("double圆周率是$pi")

    //float后要加 f 基本于Java一致
    var pai: Float = 3.1415926535897932384626f
    //float的圆周率是3.1415927 只能打印6位
    println("float的圆周率是$pai")
}