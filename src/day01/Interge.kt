package day01

/**
 * Created by young on 2017/7/5.
 * 整型 Int
 *
 */


fun main(args: Array<String>) {


    var run: Int = 6

    //kotlin 有类型推断，可以省略不写
    var runs2 = 6

    //在字符串中插入变量
    println("每天跑${runs2}公里")
}