package day01

/**
 * Created by young on 2017/7/5.
 * 可空类型 某个值可能为空的情况 用 ?
 */
fun main(args: Array<String>) {
    var addr: String? = "xx省xx市"
    var password: Int?
    password = null
    if (addr != null) {
        println("地址是$addr")
    }

    if (password != null) {
        println("密码是$password")
    }
}