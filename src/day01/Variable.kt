package day01

/**
 * Created by young on 2017/7/5.
 * 变量 var
 */

// 时间
var time = 5

/**
 * 代码注释和 Java一致
 */
fun main(args: Array<String>) {
    println(time)
    Int
}