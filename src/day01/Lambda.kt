package day01

/**
 * Created by young on 2017/7/6.
 * 高阶函数编程 lambda
 */
fun main(args: Array<String>) {
    //参数或者返回值的类型是函数
    // 无名函数 闭包

    //常见的高阶函数 map
    val a = arrayOf(1, 2, 3, 4, 5, 6)

    //it 类似于 迭代器
    val b = a.map { "这是$it" }
    for (s in b) {
        println(s)
    }

    //filter 对集合类型进行筛选
    var sum = 0
    val c = a.filter { it % 2 == 0 }.forEach {
        sum += it
    }

    println(sum)
//    for (s in c) {
//      println(s)
//    }


}