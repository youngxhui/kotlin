package day01

/**
 * Created by young on 2017/7/5.
 * 类型安全
 * 类型是定义后无法更改
 */

fun main(args: Array<String>) {
    var a: Int = 2
    //防止类型出错。
    //a = 2.4
    a = 2.4.toInt()

    //无法通过编译
    println(a)
}