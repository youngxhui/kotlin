package day02

/**
 * Created by young on 2017/7/15.
 * 内部类 嵌套类 有点卡啊
 */
class News {
    private var language = "cn"


    //新闻分类
    class Category {
        var list = arrayOf("推荐", "娱乐")

        val lists = list.joinToString()
    }
}