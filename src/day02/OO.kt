package day02

/**
 * Created by young on 2017/7/6.
 * 面向对象编程 建模 model 抽象化
 * 一个模型叫做一个类 class 简化认知，认识规律
 * 特征和属性 --变量
 * 功能和行为 function --函数
 * 继承和多态
 *3
 * 对象 实例化
 */

// 类要被继承，要用open
open class Chinese constructor(var sex: Boolean, var region: String) {
    var skin = "yello"
    var saraly: Int
        get() {
            when (region) {
                "sh" -> {
                    return 12
                }
                else -> {
                    return 10
                }
            }
        }
        set(value) {
            when (value) {
                in 10..Int.MAX_VALUE -> {
                    region = "sh"
                }
                else -> {
                    region = "sx"
                }
            }
        }
}

class Shanghainin(sex: Boolean, region: String = "sh") : Chinese(sex, region) {

}

class Shanxi(sex: Boolean, region: String) : Chinese(sex, region)

fun main(args: Array<String>) {
    val xiaoming = Shanghainin(true)
    println("小明在${xiaoming.region} ${xiaoming.saraly}")
    xiaoming.saraly -= 5
    println("小明在${xiaoming.region}")
    val xiaowang = Shanxi(false, "sx")
    println("小王在${xiaowang.region}")
}